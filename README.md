# Icomm teste para BackEnd Developer

Você está participando da rodada de contratação da Icomm, esse teste nos permite avaliar seu código e habilidades para propor arquiteturas e solucionar problemas.

## Pré requisitos

O seu teste deve ter um README com os passos necessários para:
- Rodar o projeto;
- Rodar os testes automatizados;

### Tecnologia

- Aceitaremos soluções escritas em Ruby
- Frameworks: Rails, Hanami, Sinatra
- Banco de dados: Qualquer

## Instruções

Crie uma (ou mais) branch(es) neste repositório e cria a solução nela. Faça uma Pull Request (PR) de volta para a master e nos avise que avaliamos todas as submissões.

## Requisitos

Você precisará criar uma aplicação:

1. Expor uma API seguindo os padrões REST para:
  - criar, atualizar, ativar e listar (com filtro) os Produtos;
 
2. A aplicacao devera responder as requests de criacao e atualizacao de forma assincrona, ou seja, atraves de um sistema de filas;

3. Todo Produto recém criado deve ser salvo no estado 'inativo';

4. Bonus point: O produto ter um processo de ativacao via workflow, com tratamento por estado;

### Endpoints

Os endpoints disponíveis na aplicação estão listados abaixo. Os classificados como `Protected` devem exigir autenticação, porém o método utilizado fica a seu critério.

#### Jobs

| Name       | Method    | URL                  | Protected |
| ---        | ---       | ---                  | :--:      |
| List       | GET       | /products                | X         |
| Create     | POST      | /products                | ✓         |
| Update     | PUT      | /products/{:id}           | ✓         |
| Activate   | POST      | /products/{:id}/activate | ✓         |
    
## Critério de avaliação:
- Sua API deve conter os 4 endpoints listados acima
- Sua API deve retornar **JSON** válidos em qualquer endpoint;
- Seu projeto deve ter uma suíte de testes automatizados, quanto maior a cobertura melhor;
- Não é necessário UI;
- Qualidade da abstração da solução;
- Estrutura do código e qualidade dos testes unitários;
- Projeto rodando dentro de um container.

*Bonus points:*
- Ter configurado no projeto qualquer serviço de integração contínua;
- Utilizar message queue;
- Suas respostas durante o Code Review;
- Seguir o guia de estilo padrão de mercado para a linguagem que você escolher usar;
- Um histórico do git (mesmo que breve) com mensagens claras e concisas.
